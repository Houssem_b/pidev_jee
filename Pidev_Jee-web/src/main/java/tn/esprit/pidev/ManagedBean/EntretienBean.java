package tn.esprit.pidev.ManagedBean;

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Candidature;
import model.Entrepris;
import model.Entretien;
import model.Quiz;
import tn.esprit.pidev.services.EntretienService;
import tn.esprit.pidev.services.QuizService;

@ManagedBean(name="entretienBean") 
@SessionScoped 
public class EntretienBean implements Serializable{
	private int id;
private int quiz_id;
	public int getQuiz_id() {
	return quiz_id;
}

public void setQuiz_id(int quiz_id) {
	this.quiz_id = quiz_id;
}

	private Date dateDebut;
	
	private Date dateFin;

	private float lat;

	private String location;

	private float longi;

	private String theme;

	private List<Candidature> candidatures;

	private Entrepris entrepris;

	private Quiz quiz;
	
	public List<Quiz> l_quizs;
	

	

	public List<Quiz> getL_quizs() {
		return l_quizs;
	}

	public void setL_quizs(List<Quiz> l_quizs) {
		this.l_quizs = l_quizs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getLongi() {
		return longi;
	}

	public void setLongi(float longi) {
		this.longi = longi;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<Candidature> getCandidatures() {
		return candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Entrepris getEntrepris() {
		return entrepris;
	}

	public void setEntrepris(Entrepris entrepris) {
		this.entrepris = entrepris;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	
	@EJB
	EntretienService es;
	@EJB
	QuizService qs;
	
	public void addEntretien() throws ParseException
	{ es.ajouterEntretien(new Entretien(dateDebut,dateFin,location,theme,lat)); }
	
	public void ajouterEntretien()throws ParseException
	{SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		System.out.println("hello id = "+quiz_id);
	Entretien e=new Entretien(dateDebut,dateFin,location,theme,lat);
	Quiz qu=new Quiz();
	qu.setId(quiz_id);
	e.setQuiz(qu);
	System.out.println("hello "+qu.toString());


	
	
	
      
	es.ajouterEntretien(e);
      

	}
	public List<Entretien>entretiens;

	public List<Entretien> getEntretiens() {
		return es.getAll();
	}

	public void setEntretiens(List<Entretien> entretiens) {
		this.entretiens = entretiens;
	}
	
	public List<Entretien> GetAll()
	{
		entretiens=es.getAll();
		return entretiens;
	}
	public void removeEntretien(int entretienId) 
	{ es.removeEntretien(entretienId); }
	
	public void displayEmploye(Entretien entretien) 
	{ 
		this.setDateDebut(entretien.getDateDebut()); 
		this.setDateFin(entretien.getDateFin()); 
		this.setTheme(entretien.getTheme()); 
		this.setLocation(entretien.getLocation());
		this.setId(entretien.getId());
	
	}
	public void updateEntretien() 
	{
		es.updateEntretien(new Entretien(id, dateDebut, dateFin, location,theme)); 
	} 
	@PostConstruct
	public void init() {
		
		l_quizs=es.getAllQuiz();
}
	
	

	


}
