package tn.esprit.pidev.ManagedBean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Entrepris;
import model.Entretien;
import tn.esprit.pidev.services.EntrepriseService;
import tn.esprit.pidev.services.EntretienService;
@ManagedBean(name="entrepriseBean")
@SessionScoped 
public class EntrepriseBean implements Serializable {
	private int id;
private String nomEntreprise;
private String descriptionEntre;
private int nbrEmploye;
private Date date_Created;
private Date date_Modified;
private Date date_Deleted;
private boolean isDeleted;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public boolean isDeleted() {
	return isDeleted;
}
public void setDeleted(boolean isDeleted) {
	this.isDeleted = isDeleted;
}
public String getNomEntreprise() {
	return nomEntreprise;
}
public void setNomEntreprise(String nomEntreprise) {
	this.nomEntreprise = nomEntreprise;
}
public String getDescriptionEntre() {
	return descriptionEntre;
}
public void setDescriptionEntre(String descriptionEntre) {
	this.descriptionEntre = descriptionEntre;
}
public int getNbrEmploye() {
	return nbrEmploye;
}
public void setNbrEmploye(int nbrEmploye) {
	this.nbrEmploye = nbrEmploye;
}
public Date getDate_Created() {
	return date_Created;
}
public void setDate_Created(Date date_Created) {
	this.date_Created = date_Created;
}
public Date getDate_Modified() {
	return date_Modified;
}
public void setDate_Modified(Date date_Modified) {
	this.date_Modified = date_Modified;
}
public Date getDate_Deleted() {
	return date_Deleted;
}
public void setDate_Deleted(Date date_Deleted) {
	this.date_Deleted = date_Deleted;
}
@EJB
EntrepriseService es;

public void addEntretien() throws ParseException
{
	es.ajouterEntreprise(new Entrepris(date_Created,date_Deleted,date_Modified,descriptionEntre,isDeleted,nbrEmploye,nomEntreprise));
	}

public void ajouterEntreprise()throws ParseException
{SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	
Entrepris e=new Entrepris(date_Created,date_Deleted,date_Modified,descriptionEntre,isDeleted,nbrEmploye,nomEntreprise);
e.setIsDeleted(true);
es.ajouterEntreprise(e);


}

}
