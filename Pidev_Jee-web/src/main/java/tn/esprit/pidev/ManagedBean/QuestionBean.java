package tn.esprit.pidev.ManagedBean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import model.Entrepris;
import model.Entretien;
import model.Question;
import model.Quiz;
import tn.esprit.pidev.services.QuestionService;

@ManagedBean(name="questionBean") 
@SessionScoped 
public class QuestionBean implements Serializable {
	
	private long id_Question;

	private String choice1;

	private String choice2;

	private String choice3;

	private String content_Question;

	private int correct_AnswerID;

	private Date date_Created;

	private Date date_Deleted;

	private Date date_Modified;

	private boolean isDeleted;

	private int point_Question;

	private int rank;

	private long testFK;

	private Quiz quiz;

	public long getId_Question() {
		return id_Question;
	}

	public void setId_Question(long id_Question) {
		this.id_Question = id_Question;
	}

	public String getChoice1() {
		return choice1;
	}

	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return choice2;
	}

	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return choice3;
	}

	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public String getContent_Question() {
		return content_Question;
	}

	public void setContent_Question(String content_Question) {
		this.content_Question = content_Question;
	}

	public int getCorrect_AnswerID() {
		return correct_AnswerID;
	}

	public void setCorrect_AnswerID(int correct_AnswerID) {
		this.correct_AnswerID = correct_AnswerID;
	}

	public int getPoint_Question() {
		return point_Question;
	}

	public void setPoint_Question(int point_Question) {
		this.point_Question = point_Question;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getTestFK() {
		return testFK;
	}

	public void setTestFK(long testFK) {
		this.testFK = testFK;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	@EJB
	QuestionService qs;
	
	public void addEmploye() 
	{ qs.ajouterQuestion(
			new Question( id_Question,  choice1,  choice2,  choice3,  content_Question,
			 correct_AnswerID,  point_Question,  rank,  testFK,  quiz) ); 
	
	}

	public void ajouterQuestion()throws ParseException
	{
		//SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	
//	qs.ajouterQuestion(new Question( id_Question,  choice1,  choice2,  choice3,  content_Question,
//			 correct_AnswerID,  point_Question,  rank,  testFK,  quiz) );
		qs.ajouterQuestion(new Question(content_Question,choice1,choice2,choice3));


	}
	List<Question> questions;
	
	
	public List<Question> getQuestions() {
		return questions=qs.getAllQuestion();
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public List<Question> GetAll()
	{
		questions=qs.getAllQuestion();
		return questions;
	}
	public void removeQuestion(int id) 
	{ qs.removeQuestion(id); }
	
	public void displayQuestion(Question question) 
	{ 
		this.setContent_Question(question.getContent_Question()); 
		this.setChoice1(question.getChoice1()); 
		this.setChoice2(question.getChoice2()); 
		this.setChoice3(question.getChoice3()); 
//		this.setLocation(entretien.getLocation());
	this.setId_Question(question.getId_Question());
	
	}
	public void updateQuestion() 
	{
		qs.updateQuestion(new Question(id_Question,content_Question,choice1,choice2,choice3)); 
	} 
//	@PostConstruct
//	public void init() {
//		
//		l_quizs=es.getAllQuiz();
//}


	
	
}
