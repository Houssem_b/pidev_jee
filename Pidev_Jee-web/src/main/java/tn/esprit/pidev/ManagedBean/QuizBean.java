package tn.esprit.pidev.ManagedBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import model.Entretien;
import model.Interview;
import model.Question;
import model.Quiz;
import tn.esprit.pidev.interfaces.QuizRemote;
import tn.esprit.pidev.services.QuizService;

@ManagedBean(name="quizBean") 
@SessionScoped 
public class QuizBean {
	private int id;
	private String name_Quiz;
	private int nbr_Point_Tolal;
	private int nbr_Question;
	private List<Entretien> entretiens;
	private List<Question> questions;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName_Quiz() {
		return name_Quiz;
	}
	public void setName_Quiz(String name_Quiz) {
		this.name_Quiz = name_Quiz;
	}
	public int getNbr_Point_Tolal() {
		return nbr_Point_Tolal;
	}
	public void setNbr_Point_Tolal(int nbr_Point_Tolal) {
		this.nbr_Point_Tolal = nbr_Point_Tolal;
	}
	public int getNbr_Question() {
		return nbr_Question;
	}
	public void setNbr_Question(int nbr_Question) {
		this.nbr_Question = nbr_Question;
	}
	public List<Entretien> getEntretiens() {
		return entretiens;
	}
	public void setEntretiens(List<Entretien> entretiens) {
		this.entretiens = entretiens;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	@EJB
	QuizService qs;

	public void addQuiz() throws ParseException
	{ qs.ajouterQuiz(new Quiz(name_Quiz,nbr_Point_Tolal,nbr_Point_Tolal)); }
	
	public void ajouterQuiz()throws ParseException
	{SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		
	Quiz q=new Quiz(name_Quiz,nbr_Point_Tolal,nbr_Point_Tolal);
	
	
	
	qs.ajouterQuiz(q);
	

	}
	public List<Quiz> quizs;
	
	
	public List<Quiz> getQuizs() {
		return quizs=qs.getAllQuiz();
	}
	public void setQuizs(List<Quiz> quizs) {
		this.quizs = quizs;
	}
	public List<Quiz> GetAll()
	{
		quizs=qs.getAllQuiz();
		return quizs;
	}
	public void removeQuiz(int quizId) 
	{ qs.removeQuiz(quizId); }
	
	public void displayQuiz(Quiz quiz) 
	{ 
		this.setName_Quiz(quiz.getName_Quiz()); 
	this.setNbr_Question(quiz.getNbr_Question()); 
		this.setNbr_Point_Tolal(quiz.getNbr_Point_Tolal()); 
		this.setId(quiz.getId());

	
	}
	public void updateQuiz() 
	{
		qs.updateQuiz(new Quiz(id,name_Quiz,nbr_Point_Tolal,nbr_Question)); 
	} 

}
