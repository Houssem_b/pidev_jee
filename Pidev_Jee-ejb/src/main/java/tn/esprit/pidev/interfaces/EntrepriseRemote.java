package tn.esprit.pidev.interfaces;

import javax.ejb.Remote;

import model.Entrepris;
@Remote
public interface EntrepriseRemote {
	public void ajouterEntreprise(Entrepris e);
}
