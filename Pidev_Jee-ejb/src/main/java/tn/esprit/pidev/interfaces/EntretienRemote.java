package tn.esprit.pidev.interfaces;

import java.util.List;


import javax.ejb.Remote;

import model.Entretien;
import model.Quiz;

@Remote
public interface EntretienRemote {
	 public void ajouterEntretien (Entretien e);
	 public List<Entretien> getAll();
	 public void removeEntretien(int id);
	 public void updateEntretien(Entretien user);
	 public void findById(int id);
	 public List<Quiz > getAllQuiz();


}
