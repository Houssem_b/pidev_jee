package tn.esprit.pidev.interfaces;

import javax.ejb.Local;

import model.Entretien;

@Local
public interface EntretienLocal {
	 public void ajouterEntretien (Entretien e);
}
