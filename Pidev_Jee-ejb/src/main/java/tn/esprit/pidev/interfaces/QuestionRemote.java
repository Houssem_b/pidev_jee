package tn.esprit.pidev.interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.Question;
import model.Quiz;

@Remote
public interface QuestionRemote {
	
	 public void ajouterQuestion (Question question);
	 public List<Question> getAllQuestion();
	 public void removeQuestion(long id);
	 public void updateQuestion(Question question);
	 public void findById(int id);

}
