package tn.esprit.pidev.interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.Question;
import model.Quiz;

@Remote
public interface QuizRemote {
	 public void ajouterQuiz (Quiz quiz);
	 public List<Quiz> getAllQuiz();
	 public void removeQuiz(int id);
	 public void updateQuiz(Quiz quiz);
	 public void findById(int id);

}
