package tn.esprit.pidev.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Entretien;
import model.Quiz;
import tn.esprit.pidev.interfaces.QuizRemote;
@Stateless
@LocalBean
public class QuizService implements QuizRemote{
	 @PersistenceContext //(unitName="Pidev_Jee-ejb")
		EntityManager em;

	@Override
	public void ajouterQuiz(Quiz quiz) {
    em.persist(quiz);		
	}

	@Override
	public List<Quiz> getAllQuiz() {
		List<Quiz> quizs =em.createQuery("Select q from Quiz q",Quiz.class).getResultList();
		return quizs;
	}

	@Override
	public void removeQuiz(int id) {
		em.remove(em.find(Quiz.class, id));
		
	}

	@Override
	public void updateQuiz(Quiz quiz) {
		Quiz q = em.find(Quiz.class, quiz.getId()); 
		q.setNbr_Question(quiz.getNbr_Question()); 
		q.setNbr_Point_Tolal(quiz.getNbr_Point_Tolal()); 
		q.setName_Quiz(quiz.getName_Quiz());
		q.setId(quiz.getId());
	}

	@Override
	public void findById(int id) {
		Quiz q = em.find(Quiz.class, id);
				
	}
}
