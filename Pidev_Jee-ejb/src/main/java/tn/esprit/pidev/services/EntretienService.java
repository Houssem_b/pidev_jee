package tn.esprit.pidev.services;

import java.util.List;



import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Entrepris;
import model.Entretien;
import model.Quiz;
import tn.esprit.pidev.interfaces.EntretienRemote;

@Stateless
@LocalBean
public class EntretienService implements EntretienRemote{

	 @PersistenceContext //(unitName="Pidev_Jee-ejb")
		EntityManager em;
	@Override
	public void ajouterEntretien(Entretien e) {
		// TODO Auto-generated method stub
		Entrepris entrepris = em.find(Entrepris.class, 6);
		e.setEntrepris(entrepris);
		e.setIsDeleted(false);
		em.persist(e);
		
		
		
		
	}
	@Override
	public List<Entretien> getAll() {
		List<Entretien> entretiens =em.createQuery("Select e from Entretien e",Entretien.class).getResultList();
		
		return entretiens;
	}
	@Override
	public void removeEntretien(int id) {
		em.remove(em.find(Entretien.class, id));
		
	}
	@Override
	public void updateEntretien(Entretien entretien) {
		Entretien emp = em.find(Entretien.class, entretien.getId()); 
		emp.setDateDebut(entretien.getDateDebut()); 
		emp.setDateFin(entretien.getDateFin()); 
		emp.setTheme(entretien.getTheme());
		emp.setLocation(entretien.getLocation());
		//emp.setId(entretien.getId());
	//	emp.setIsActif();
		
	}
	@Override
	public void findById(int id) {
		System.out.println("In findById : ");
		Entretien emp = em.find(Entretien.class, id);
		System.out.println("Out of findById : ");
		System.out.println(emp.toString());		
	}
	@Override 
	public List<Quiz> getAllQuiz()
	{ List<Quiz> quiz = em.createQuery("Select c from Quiz c", Quiz.class).getResultList(); return quiz; 
	
	}
}
