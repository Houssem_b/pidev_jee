package tn.esprit.pidev.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Question;
import tn.esprit.pidev.interfaces.QuestionRemote;

@Stateless
@LocalBean
public class QuestionService implements QuestionRemote {
	 @PersistenceContext //(unitName="Pidev_Jee-ejb")
		EntityManager em;




	@Override
	public void ajouterQuestion(Question question) {
     em.persist(question);		
	}

	@Override
	public List<Question> getAllQuestion() {
		List<Question> question =em.createQuery("Select e from Question e",Question.class).getResultList();
		return question;
	}

	@Override
	public void removeQuestion(long id) {
		em.remove(em.find(Question.class, id));
		
	}

	@Override
	public void updateQuestion(Question question) {
		
	Question q = em.find(Question.class, question.getId_Question()); 
	q.setId_Question(question.getId_Question());
	q.setContent_Question(question.getContent_Question()); 
	q.setChoice1(question.getChoice1()); 
	q.setChoice2(question.getChoice2()); 
	q.setChoice3(question.getChoice3()); 
	q.setPoint_Question(question.getPoint_Question()); 


		
	}

	@Override
	public void findById(int id) {
		Question que = em.find(Question.class, id);
		
	}

}
