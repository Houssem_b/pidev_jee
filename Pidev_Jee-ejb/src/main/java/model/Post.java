package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the Posts database table.
 * 
 */
@Entity
@Table(name="Posts")
@NamedQuery(name="Post.findAll", query="SELECT p FROM Post p")
public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="body_post")
	private String bodyPost;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	private String title;

	public Post() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBodyPost() {
		return this.bodyPost;
	}

	public void setBodyPost(String bodyPost) {
		this.bodyPost = bodyPost;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}