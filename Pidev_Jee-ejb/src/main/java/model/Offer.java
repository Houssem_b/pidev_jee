package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Offers database table.
 * 
 */
@Entity
@Table(name="Offers")
@NamedQuery(name="Offer.findAll", query="SELECT o FROM Offer o")
public class Offer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	private Date dateoffer;

	@Column(name="descrip_offer")
	private String descripOffer;

	private String etat;

	@Column(name="experience_offer")
	private String experienceOffer;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	private String specialite;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="offer")
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Interview
	@OneToMany(mappedBy="offer")
	private List<Interview> interviews;

	//bi-directional many-to-one association to Entrepris
	@ManyToOne
	@JoinColumn(name="Entreprise_Id")
	private Entrepris entrepris;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="offers")
	private List<User> users;

	public Offer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public Date getDateoffer() {
		return this.dateoffer;
	}

	public void setDateoffer(Date dateoffer) {
		this.dateoffer = dateoffer;
	}

	public String getDescripOffer() {
		return this.descripOffer;
	}

	public void setDescripOffer(String descripOffer) {
		this.descripOffer = descripOffer;
	}

	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getExperienceOffer() {
		return this.experienceOffer;
	}

	public void setExperienceOffer(String experienceOffer) {
		this.experienceOffer = experienceOffer;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getSpecialite() {
		return this.specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setOffer(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setOffer(null);

		return candidature;
	}

	public List<Interview> getInterviews() {
		return this.interviews;
	}

	public void setInterviews(List<Interview> interviews) {
		this.interviews = interviews;
	}

	public Interview addInterview(Interview interview) {
		getInterviews().add(interview);
		interview.setOffer(this);

		return interview;
	}

	public Interview removeInterview(Interview interview) {
		getInterviews().remove(interview);
		interview.setOffer(null);

		return interview;
	}

	public Entrepris getEntrepris() {
		return this.entrepris;
	}

	public void setEntrepris(Entrepris entrepris) {
		this.entrepris = entrepris;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}