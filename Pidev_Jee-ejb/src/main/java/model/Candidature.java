package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Candidatures database table.
 * 
 */
@Entity
@Table(name="Candidatures")
@NamedQuery(name="Candidature.findAll", query="SELECT c FROM Candidature c")
public class Candidature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id_candidature")
	private int id_candidature;

	@Column(name="Etat")
	private String etat;

	@Column(name="PathCV")
	private String pathCV;

	//bi-directional many-to-one association to Entretien
	@ManyToOne
	@JoinColumn(name="Id")
	private Entretien entretien;

	//bi-directional many-to-one association to Offer
	@ManyToOne
	@JoinColumn(name="offrefk")
	private Offer offer;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="candidafk")
	private User user;

	public Candidature() {
	}

	public int getId_candidature() {
		return this.id_candidature;
	}

	public void setId_candidature(int id_candidature) {
		this.id_candidature = id_candidature;
	}

	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getPathCV() {
		return this.pathCV;
	}

	public void setPathCV(String pathCV) {
		this.pathCV = pathCV;
	}

	public Entretien getEntretien() {
		return this.entretien;
	}

	public void setEntretien(Entretien entretien) {
		this.entretien = entretien;
	}

	public Offer getOffer() {
		return this.offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}