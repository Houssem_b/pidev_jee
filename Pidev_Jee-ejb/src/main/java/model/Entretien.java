package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Entretien database table.
 * 
 */
@Entity
@NamedQuery(name="Entretien.findAll", query="SELECT e FROM Entretien e")
public class Entretien implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	@Column(name="DateDebut")
	private Date dateDebut;

	@Column(name="DateFin")
	private Date dateFin;

	@Column(name="IsDeleted")
	private boolean isDeleted;
	@Column(name="lat")

	private float lat;

	@Column(name="Location")
	private String location;
	@Column(name="longi")
	private float longi;

	private String theme;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="entretien")
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Entrepris
	@ManyToOne
	@JoinColumn(name="entreprisefk")
	private Entrepris entrepris;

	//bi-directional many-to-one association to Quiz
	@ManyToOne
	private Quiz quiz;

	public Entretien() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public Date getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public float getLat() {
		return this.lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}*/

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/*public float getLongi() {
		return this.longi;
	}

	public void setLongi(float longi) {
		this.longi = longi;
	}*/

	public String getTheme() {
		return this.theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setEntretien(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setEntretien(null);

		return candidature;
	}

	public Entrepris getEntrepris() {
		return this.entrepris;
	}

	public void setEntrepris(Entrepris entrepris) {
		this.entrepris = entrepris;
	}

	public Quiz getQuiz() {
		return this.quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	public Entretien(Date dateDebut, Date dateFin, String location, String theme,float lat) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.location = location;
		this.theme = theme;
		this.lat=40;
		
	}
	public Entretien(int id,Date dateDebut, Date dateFin, String location, String theme ){
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.location = location;
		this.theme = theme;
		
	}
}