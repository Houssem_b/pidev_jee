package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Quizs database table.
 * 
 */
@Entity
@Table(name="Quizs")
@NamedQuery(name="Quiz.findAll", query="SELECT q FROM Quiz q")
public class Quiz implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	@Column(name="Name_Quiz")
	private String name_Quiz;

	@Column(name="Nbr_Point_Tolal")
	private int nbr_Point_Tolal;

	@Column(name="Nbr_Question")
	private int nbr_Question;

	//bi-directional many-to-one association to Entretien
	@OneToMany(mappedBy="quiz")
	private List<Entretien> entretiens;

	//bi-directional many-to-one association to Interview
	@OneToMany(mappedBy="quiz")
	private List<Interview> interviews;

	//bi-directional many-to-one association to Question
	@OneToMany(mappedBy="quiz")
	private List<Question> questions;

	public Quiz() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName_Quiz() {
		return this.name_Quiz;
	}

	public void setName_Quiz(String name_Quiz) {
		this.name_Quiz = name_Quiz;
	}

	public int getNbr_Point_Tolal() {
		return this.nbr_Point_Tolal;
	}

	public void setNbr_Point_Tolal(int nbr_Point_Tolal) {
		this.nbr_Point_Tolal = nbr_Point_Tolal;
	}

	public int getNbr_Question() {
		return this.nbr_Question;
	}

	public void setNbr_Question(int nbr_Question) {
		this.nbr_Question = nbr_Question;
	}

	public List<Entretien> getEntretiens() {
		return this.entretiens;
	}

	public void setEntretiens(List<Entretien> entretiens) {
		this.entretiens = entretiens;
	}

	public Entretien addEntretien(Entretien entretien) {
		getEntretiens().add(entretien);
		entretien.setQuiz(this);

		return entretien;
	}

	public Entretien removeEntretien(Entretien entretien) {
		getEntretiens().remove(entretien);
		entretien.setQuiz(null);

		return entretien;
	}

	public List<Interview> getInterviews() {
		return this.interviews;
	}

	public void setInterviews(List<Interview> interviews) {
		this.interviews = interviews;
	}

	public Interview addInterview(Interview interview) {
		getInterviews().add(interview);
		interview.setQuiz(this);

		return interview;
	}

	public Interview removeInterview(Interview interview) {
		getInterviews().remove(interview);
		interview.setQuiz(null);

		return interview;
	}

	public List<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Question addQuestion(Question question) {
		getQuestions().add(question);
		question.setQuiz(this);

		return question;
	}

	public Question removeQuestion(Question question) {
		getQuestions().remove(question);
		question.setQuiz(null);

		return question;
	}

	public Quiz(String name_Quiz, int nbr_Point_Tolal, int nbr_Question) {
		super();
		this.name_Quiz = name_Quiz;
		this.nbr_Point_Tolal = nbr_Point_Tolal;
		this.nbr_Question = nbr_Question;
	}
	public Quiz(int id,String name_Quiz, int nbr_Point_Tolal, int nbr_Question) {
		super();
		this.id=id;
		this.name_Quiz = name_Quiz;
		this.nbr_Point_Tolal = nbr_Point_Tolal;
		this.nbr_Question = nbr_Question;
	}

	@Override
	public String toString() {
		return "Quiz [name_Quiz=" + name_Quiz + ", nbr_Question=" + nbr_Question + "]";
	}
	

}