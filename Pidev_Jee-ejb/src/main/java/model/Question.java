package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the Questions database table.
 * 
 */
@Entity
@Table(name="Questions")
@NamedQuery(name="Question.findAll", query="SELECT q FROM Question q")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id_Question")
	private long id_Question;

	private String choice1;

	private String choice2;

	private String choice3;

	@Column(name="Content_Question")
	private String content_Question;

	@Column(name="Correct_AnswerID")
	private int correct_AnswerID;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	@Column(name="Point_Question")
	private int point_Question;

	@Column(name="Rank")
	private int rank;

	@Column(name="TestFK")
	private long testFK;

	//bi-directional many-to-one association to Quiz
	@ManyToOne
	@JoinColumn(name="Quiz_Id")
	private Quiz quiz;

	public Question() {
	}

	public long getId_Question() {
		return this.id_Question;
	}

	public void setId_Question(long id_Question) {
		this.id_Question = id_Question;
	}

	public String getChoice1() {
		return this.choice1;
	}

	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return this.choice2;
	}

	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return this.choice3;
	}

	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public String getContent_Question() {
		return this.content_Question;
	}

	public void setContent_Question(String content_Question) {
		this.content_Question = content_Question;
	}

	public int getCorrect_AnswerID() {
		return this.correct_AnswerID;
	}

	public void setCorrect_AnswerID(int correct_AnswerID) {
		this.correct_AnswerID = correct_AnswerID;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getPoint_Question() {
		return this.point_Question;
	}

	public void setPoint_Question(int point_Question) {
		this.point_Question = point_Question;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getTestFK() {
		return this.testFK;
	}

	public void setTestFK(long testFK) {
		this.testFK = testFK;
	}

	public Quiz getQuiz() {
		return this.quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	public Question(long id_Question, String choice1, String choice2, String choice3, String content_Question,
			int correct_AnswerID, int point_Question, int rank, long testFK, Quiz quiz) {
		super();
		this.id_Question = id_Question;
		this.choice1 = choice1;
		this.choice2 = choice2;
		this.choice3 = choice3;
		this.content_Question = content_Question;
		this.correct_AnswerID = correct_AnswerID;
		this.point_Question = point_Question;
		this.rank = rank;
		this.testFK = testFK;
		this.quiz = quiz;
	}

	public Question(  String content_Question,String choice1, String choice2, String choice3) {
		super();
		this.id_Question = id_Question;
		this.choice1 = choice1;
		this.choice2 = choice2;
		this.choice3 = choice3;
		this.content_Question = content_Question;
	}
	public Question(long id,  String content_Question,String choice1, String choice2, String choice3) {
		super();
		
		this.id_Question = id;
		this.choice1 = choice1;
		this.choice2 = choice2;
		this.choice3 = choice3;
		this.content_Question = content_Question;
	}
	
	
}