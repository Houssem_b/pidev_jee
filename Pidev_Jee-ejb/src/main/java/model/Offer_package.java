package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Offer_package database table.
 * 
 */
@Entity
@NamedQuery(name="Offer_package.findAll", query="SELECT o FROM Offer_package o")
public class Offer_package implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	private String detail;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	private double prix;

	//bi-directional many-to-many association to Entrepris
	@ManyToMany
	@JoinTable(
		name="EntreprisePack"
		, joinColumns={
			@JoinColumn(name="PackageRefId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="EntrepriseRefId")
			}
		)
	private List<Entrepris> entreprises;

	public Offer_package() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public List<Entrepris> getEntreprises() {
		return this.entreprises;
	}

	public void setEntreprises(List<Entrepris> entreprises) {
		this.entreprises = entreprises;
	}

}