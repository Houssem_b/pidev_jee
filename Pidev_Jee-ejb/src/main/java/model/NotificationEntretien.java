package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the NotificationEntretiens database table.
 * 
 */
@Entity
@Table(name="NotificationEntretiens")
@NamedQuery(name="NotificationEntretien.findAll", query="SELECT n FROM NotificationEntretien n")
public class NotificationEntretien implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	private String description;

	public NotificationEntretien() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}