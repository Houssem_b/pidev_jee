package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Entreprises database table.
 * 
 */
@Entity
@Table(name="Entreprises")
@NamedQuery(name="Entrepris.findAll", query="SELECT e FROM Entrepris e")
public class Entrepris implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Date_Created")
	private Date date_Created;

	@Column(name="Date_Deleted")
	private Date date_Deleted;

	@Column(name="Date_Modified")
	private Date date_Modified;

	@Column(name="description_entre")
	private String descriptionEntre;

	//private String etat;

	@Column(name="IsDeleted")
	private boolean isDeleted;

	//private String logo;

	@Column(name="nbr_employe")
	private int nbrEmploye;

	@Column(name="nom_entreprise")
	private String nomEntreprise;

	/*@Column(name="Secteur")
	private String secteur;*/

	//bi-directional many-to-one association to Entretien
	@OneToMany(mappedBy="entrepris")
	private List<Entretien> entretiens;

	/*//bi-directional many-to-one association to Interview
	@OneToMany(mappedBy="entrepris")
	private List<Interview> interviews;

	//bi-directional many-to-many association to Offer_package
	@ManyToMany(mappedBy="entreprises")
	private List<Offer_package> offerPackages;

	//bi-directional many-to-one association to Offer
	@OneToMany(mappedBy="entrepris")
	private List<Offer> offers;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="entrepris")
	private List<User> users;*/

	public Entrepris() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_Created() {
		return this.date_Created;
	}

	public void setDate_Created(Date date_Created) {
		this.date_Created = date_Created;
	}

	public Date getDate_Deleted() {
		return this.date_Deleted;
	}

	public void setDate_Deleted(Date date_Deleted) {
		this.date_Deleted = date_Deleted;
	}

	public Date getDate_Modified() {
		return this.date_Modified;
	}

	public void setDate_Modified(Date date_Modified) {
		this.date_Modified = date_Modified;
	}

	public String getDescriptionEntre() {
		return this.descriptionEntre;
	}

	public void setDescriptionEntre(String descriptionEntre) {
		this.descriptionEntre = descriptionEntre;
	}

	

	


	/*public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}*/
	
	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}*/

	public int getNbrEmploye() {
		return this.nbrEmploye;
	}

	public void setNbrEmploye(int nbrEmploye) {
		this.nbrEmploye = nbrEmploye;
	}

	public String getNomEntreprise() {
		return this.nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	

	public Entrepris( Date date_Created, Date date_Deleted, Date date_Modified, String descriptionEntre,
			boolean isDeleted, int nbrEmploye, String nomEntreprise) {
		super();
		
		this.date_Created = date_Created;
		this.date_Deleted = date_Deleted;
		this.date_Modified = date_Modified;
		this.descriptionEntre = descriptionEntre;
		this.isDeleted = isDeleted;
		this.nbrEmploye = nbrEmploye;
		this.nomEntreprise = nomEntreprise;
	}

	/*public String getSecteur() {
		return this.secteur;
	}

	public void setSecteur(String secteur) {
		this.secteur = secteur;
	}*/

	public List<Entretien> getEntretiens() {
		return this.entretiens;
	}

	public void setEntretiens(List<Entretien> entretiens) {
		this.entretiens = entretiens;
	}

	public Entretien addEntretien(Entretien entretien) {
		getEntretiens().add(entretien);
		entretien.setEntrepris(this);

		return entretien;
	}

	public Entretien removeEntretien(Entretien entretien) {
		getEntretiens().remove(entretien);
		entretien.setEntrepris(null);

		return entretien;
	}

	/*public List<Interview> getInterviews() {
		return this.interviews;
	}

	public void setInterviews(List<Interview> interviews) {
		this.interviews = interviews;
	}

	public Interview addInterview(Interview interview) {
		getInterviews().add(interview);
		interview.setEntrepris(this);

		return interview;
	}

	public Interview removeInterview(Interview interview) {
		getInterviews().remove(interview);
		interview.setEntrepris(null);

		return interview;
	}

	public List<Offer_package> getOfferPackages() {
		return this.offerPackages;
	}

	public void setOfferPackages(List<Offer_package> offerPackages) {
		this.offerPackages = offerPackages;
	}

	public List<Offer> getOffers() {
		return this.offers;
	}

	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	public Offer addOffer(Offer offer) {
		getOffers().add(offer);
		offer.setEntrepris(this);

		return offer;
	}

	public Offer removeOffer(Offer offer) {
		getOffers().remove(offer);
		offer.setEntrepris(null);

		return offer;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setEntrepris(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setEntrepris(null);

		return user;
	}*/

}